package com.caja.fritzgerald.elective4;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.card.MaterialCardView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.caja.fritzgerald.elective4.Utils.GameUtils;
import com.caja.fritzgerald.elective4.Utils.SharedPreferencesManager;

public class MainActivity extends AppCompatActivity {

    MaterialCardView mcv_newgame, mcv_resumegame, mcv_highscore;
    ConstraintLayout cl_mainactivity;
    Snackbar snackbar;

    SharedPreferencesManager sharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mcv_newgame = findViewById(R.id.mcv_newgame);
        mcv_resumegame = findViewById(R.id.mcv_resumegame);
        //mcv_highscore = findViewById(R.id.mcv_highscore);

        cl_mainactivity = findViewById(R.id.cl_mainactivity);

        snackbar = Snackbar.make(cl_mainactivity, "Press back again to exit the game.", Snackbar.LENGTH_SHORT);

        sharedPreferencesManager = new SharedPreferencesManager(MainActivity.this);

        if (sharedPreferencesManager.getProgressId() <= 0) {
            mcv_resumegame.setEnabled(false);
        } else {
            mcv_resumegame.setEnabled(true);
        }

        mcv_newgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sharedPreferencesManager.isSharedPrefExisting()) {
                    Log.d(GameUtils.LOG_TAG, "progressId value is null");

                    navigateToGameActivity(true);

                } else {

                    if (!sharedPreferencesManager.getProgressDifficulty().equals("easy")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Previous game has been found. Do you want to resume instead ?")
                                .setPositiveButton("Resume", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigateToSelectCheckpoint();
                                    }
                                })
                                .setNegativeButton("New game", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigateToGameActivity(true);
                                    }
                                });

                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    } else {
                        navigateToGameActivity(true);
                    }


                }
            }
        });

        mcv_resumegame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //navigateToGameActivity(false);
                navigateToSelectCheckpoint();
            }
        });

        //if(sharedPreferencesManager.getHasProgress().isEmpty() || sharedPreferencesManager.getHasProgress() == null){

    }

    private void navigateToSelectCheckpoint() {
        startActivity(new Intent(MainActivity.this, CheckpointActivity.class));
        finish();
    }

    private void navigateToGameActivity(boolean isNewgame) {
        Intent intent = new Intent(MainActivity.this, PlayActivity.class);

        Bundle bundle = new Bundle();
        bundle.putBoolean(GameUtils.BUNDLE_ISNEWGAME, isNewgame);
        intent.putExtras(bundle);

        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        if (snackbar.isShown()) {
            super.onBackPressed();
        } else {
            snackbar.show();
        }
    }
}
