package com.caja.fritzgerald.elective4.SQLiteAdapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by John Steven Frio on 12/14/2018.
 */

public class SQLiteAdapter {
    SQLiteHelper sqLiteHelper;
    SQLiteDatabase sqLiteDatabase;

    public SQLiteAdapter(Context context) {
        sqLiteHelper = new SQLiteHelper(context);
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
    }

    public long insertData(String difficulty, String questions, String correct, String incorrect1, String incorrect2, String incorrect3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLiteHelper.COL_Difficulty, difficulty);
        contentValues.put(SQLiteHelper.COL_Questions, questions);
        contentValues.put(SQLiteHelper.COL_Correct, correct);
        contentValues.put(SQLiteHelper.COL_Incorrect1, incorrect1);
        contentValues.put(SQLiteHelper.COL_Incorrect2, incorrect2);
        contentValues.put(SQLiteHelper.COL_Incorrect3, incorrect3);

        long id = sqLiteDatabase.insert(SQLiteHelper.TABLE_NAME, null, contentValues);
        return id;

    }

    public String selectQuestion(int id, String difficulty) {
        String selectQuery = "SELECT " + SQLiteHelper.COL_Questions + " FROM " + SQLiteHelper.TABLE_NAME + " WHERE " + SQLiteHelper.COL_ID + " = '" + id + "' AND " + SQLiteHelper.COL_Difficulty + " = '" + difficulty + "'";

        String question = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            question = cursor.getString(0);
        }

        cursor.close();

        return question;
    }

    public String selectCorrect(int id, String difficulty) {
        String selectQuery = "SELECT " + SQLiteHelper.COL_Correct + " FROM " + SQLiteHelper.TABLE_NAME + " WHERE " + SQLiteHelper.COL_ID + " = '" + id + "' AND " + SQLiteHelper.COL_Difficulty + " = '" + difficulty + "'";

        String correct = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            correct = cursor.getString(0);
        }

        cursor.close();

        return correct;
    }

    public String selectIncorrect1(int id, String difficulty) {

        String selectQuery = "SELECT " + SQLiteHelper.COL_Incorrect1 + " FROM " + SQLiteHelper.TABLE_NAME + " WHERE " + SQLiteHelper.COL_ID + " = '" + id + "' AND " + SQLiteHelper.COL_Difficulty + " = '" + difficulty + "'";

        String incorrect1 = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            incorrect1 = cursor.getString(0);
        }

        cursor.close();

        return incorrect1;
    }

    public String selectIncorrect2(int id, String difficulty) {

        String selectQuery = "SELECT " + SQLiteHelper.COL_Incorrect2 + " FROM " + SQLiteHelper.TABLE_NAME + " WHERE " + SQLiteHelper.COL_ID + " = '" + id + "' AND " + SQLiteHelper.COL_Difficulty + " = '" + difficulty + "'";

        String incorrect2 = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            incorrect2 = cursor.getString(0);
        }

        cursor.close();

        return incorrect2;
    }

    public String selectIncorrect3(int id, String difficulty) {

        String selectQuery = "SELECT " + SQLiteHelper.COL_Incorrect3 + " FROM " + SQLiteHelper.TABLE_NAME + " WHERE " + SQLiteHelper.COL_ID + " = '" + id + "' AND " + SQLiteHelper.COL_Difficulty + " = '" + difficulty + "'";

        String incorrect3 = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            incorrect3 = cursor.getString(0);
        }

        cursor.close();

        return incorrect3;
    }

    public int countRecords() {
        String countQuery = "SELECT * FROM " + SQLiteHelper.TABLE_NAME + "";
        Cursor cursor = sqLiteDatabase.rawQuery(countQuery, null);
        int count = cursor.getCount();

        cursor.close();
        return count;


    }

    public static class SQLiteHelper extends SQLiteOpenHelper {

        public static final String DATABASE_NAME = "TriviaQuestions.db";
        public static final int DATABASE_VERSION = 1;
        public static final String TABLE_NAME = "TriviaQuestionsTable";
        public static final String COL_ID = "ID";
        public static final String COL_Difficulty = "Difficulty";
        public static final String COL_Questions = "Questions";
        public static final String COL_Correct = "Correct";
        public static final String COL_Incorrect1 = "Incorrect1";
        public static final String COL_Incorrect2 = "Incorrect2";
        public static final String COL_Incorrect3 = "Incorrect3";
        private static final String CreateQuery = "CREATE TABLE " + TABLE_NAME + "(" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_Difficulty + ", " + COL_Questions + ", " + COL_Correct + ", " + COL_Incorrect1 + ", " + COL_Incorrect2 + ", " + COL_Incorrect3 + ")";
        private static final String DropQuery = "DROP TABLE IF EXISTS " + TABLE_NAME + "";


        public SQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CreateQuery);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DropQuery);
        }
    }
}
