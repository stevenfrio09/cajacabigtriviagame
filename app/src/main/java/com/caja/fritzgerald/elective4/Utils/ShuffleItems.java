package com.caja.fritzgerald.elective4.Utils;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.caja.fritzgerald.elective4.PlayActivity;

import java.util.Arrays;
import java.util.Random;


/**
 * Created by John Steven Frio on 12/16/2018.
 */

public class ShuffleItems {

    Context context;

    public ShuffleItems(Context context) {
        this.context = context;
    }


    public void shuffleQuestions(int start, int end, int items, String difficulty) {

        int[] array = new int[items];

        for (int i = start; i < end; i++) {
            switch (difficulty) {
                case "easy":
                    array[i] = i + 1;
                    break;
                case "medium":
                    array[i] = i + 31;
                    break;
                case "hard":
                    array[i] = i + 51;
            }

        }

        Log.d(GameUtils.LOG_TAG, "unshuffledEasy : " + difficulty + " : " + Arrays.toString(array));
        FisherYatesIntegerShuffle(array);
        Log.d(GameUtils.LOG_TAG, "shuffledEasy : " + difficulty + " : " + Arrays.toString(array));


        new SharedPreferencesManager(context).setShuffledNumbers(array);

    }

    public void shuffleChoices(String correct, String incorrect1, String incorrect2, String incorrect3, TextView tv_option1, TextView tv_option2, TextView tv_option3, TextView tv_option4){
        String[] choices = {correct, incorrect1, incorrect2, incorrect3};
        FisherYatesStringShuffle(choices);

        tv_option1.setText(choices[0]);
        tv_option2.setText(choices[1]);
        tv_option3.setText(choices[2]);
        tv_option4.setText(choices[3]);
    }

    //FisherYatesIntegerShuffle algorithm, source: stackoverflow hehe
    private void FisherYatesIntegerShuffle(int[] array) {
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            int index = random.nextInt(i);

            int tmp = array[index];
            array[index] = array[i];
            array[i] = tmp;
        }
    }

    private void FisherYatesStringShuffle(String[] array) {
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            int index = random.nextInt(i);

            String tmp = array[index];
            array[index] = array[i];
            array[i] = tmp;
        }
    }
}
