package com.caja.fritzgerald.elective4;

import android.content.Intent;
import android.support.design.card.MaterialCardView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.caja.fritzgerald.elective4.Utils.GameUtils;
import com.caja.fritzgerald.elective4.Utils.SharedPreferencesManager;

public class RoundCleared extends AppCompatActivity {

    TextView tv_roundCleared, tv_roundCleared_home, tv_roundCleared_continue;
    MaterialCardView mcv_roundCleared_continue;
    SharedPreferencesManager sharedPreferencesManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_round_cleared);

        tv_roundCleared = findViewById(R.id.tv_roundCleared);
        tv_roundCleared_continue = findViewById(R.id.tv_roundCleared_continue);
        tv_roundCleared_home = findViewById(R.id.tv_roundCleared_home);
        mcv_roundCleared_continue = findViewById(R.id.mcv_roundCleared_continue);

        sharedPreferencesManager = new SharedPreferencesManager(this);

        String difficulty ="";
        String roundClear = "";

        Bundle bundle = getIntent().getExtras();

        switch (bundle.getString(GameUtils.BUNDLE_DIFFICULTY)){
            case "easy":
                difficulty = "easy";
                roundClear = "You have cleared the " + difficulty + " round.";
                break;
            case "medium":
                difficulty = "medium";
                roundClear = "You have cleared the " + difficulty + " round.";
                break;
            case "hard":
                difficulty = "hard";
                roundClear = "You have cleared the " + difficulty + " round. Congratulations!";
                mcv_roundCleared_continue.setVisibility(View.GONE);
                break;


        }

        tv_roundCleared.setText(roundClear);

        tv_roundCleared_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RoundCleared.this, MainActivity.class));
                finish();
            }
        });

        tv_roundCleared_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RoundCleared.this, PlayActivity.class);

                Bundle bundle = new Bundle();
                bundle.putBoolean(GameUtils.BUNDLE_ISNEWGAME, false);
                intent.putExtras(bundle);

                startActivity(intent);

                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(RoundCleared.this, MainActivity.class));
        finish();
    }
}
