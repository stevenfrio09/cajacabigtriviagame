package com.caja.fritzgerald.elective4;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.card.MaterialCardView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.caja.fritzgerald.elective4.Utils.GameUtils;
import com.caja.fritzgerald.elective4.Utils.SharedPreferencesManager;

public class CheckpointActivity extends AppCompatActivity {

    MaterialCardView mcv_easy, mcv_medium, mcv_hard;
    TextView tv_easy, tv_medium, tv_hard;

    SharedPreferencesManager sharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkpoint);

        mcv_easy = findViewById(R.id.mcv_easy);
        mcv_medium = findViewById(R.id.mcv_medium);
        mcv_hard = findViewById(R.id.mcv_hard);
        tv_easy = findViewById(R.id.tv_easy);
        tv_medium = findViewById(R.id.tv_medium);
        tv_hard = findViewById(R.id.tv_hard);

        sharedPreferencesManager = new SharedPreferencesManager(CheckpointActivity.this);

        onClickListeners(mcv_easy, "easy");
        onClickListeners(mcv_medium, "medium");
        onClickListeners(mcv_hard, "hard");

        switch (sharedPreferencesManager.getProgressDifficulty()) {
            case "easy":

                activateButton(mcv_easy, tv_easy);

                deactivateButton(mcv_medium, tv_medium);
                deactivateButton(mcv_hard, tv_hard);
                break;

            case "medium":

                activateButton(mcv_easy, tv_easy);
                activateButton(mcv_medium, tv_medium);

                deactivateButton(mcv_hard, tv_hard);

                break;
            case "hard":

                activateButton(mcv_easy, tv_easy);
                activateButton(mcv_medium, tv_medium);
                activateButton(mcv_hard, tv_hard);

                break;
        }
    }

    private void onClickListeners(final MaterialCardView materialCardView, final String difficulty) {
        materialCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (difficulty) {
                    case "easy":
                        sharedPreferencesManager.setProgressDifficulty("easy");
                        sharedPreferencesManager.setProgressId(1);
                        break;
                    case "medium":
                        sharedPreferencesManager.setProgressDifficulty("medium");
                        sharedPreferencesManager.setProgressId(1);
                        break;
                    case "hard":
                        sharedPreferencesManager.setProgressDifficulty("hard");
                        sharedPreferencesManager.setProgressId(1);
                        break;
                }

                Intent intent = new Intent(CheckpointActivity.this, PlayActivity.class);

                Bundle bundle = new Bundle();
                bundle.putBoolean(GameUtils.BUNDLE_ISNEWGAME, false);
                intent.putExtras(bundle);

                startActivity(intent);
                finish();


            }
        });
    }

    private void activateButton(MaterialCardView materialCardView, TextView textView) {
        materialCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
        textView.setTextColor(getResources().getColor(R.color.bg_main));

        materialCardView.setEnabled(true);
    }

    private void deactivateButton(MaterialCardView materialCardView, TextView textView) {
        materialCardView.setCardBackgroundColor(getResources().getColor(R.color.bg_main));
        textView.setTextColor(getResources().getColor(R.color.white));

        materialCardView.setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(CheckpointActivity.this, MainActivity.class));
        finish();
    }
}
