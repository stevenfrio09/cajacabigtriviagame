package com.caja.fritzgerald.elective4.TriviaQA;

import android.content.Context;

import com.caja.fritzgerald.elective4.SQLiteAdapter.SQLiteAdapter;

/**
 * Created by John Steven Frio on 12/14/2018.
 */

public class TriviaQA {

    public static final String[] difficulty = {
            //1-30 ---- 30 ITEMS
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",
            "easy",

            //31-50 ------ 20 ITEMS

            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",
            "medium",

            //51-65 --------- 15 ITEMS
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard",
            "hard"
    };

    public static final String[] questions = {
            "Ano ang ‘monetary unit’ ng Pilipinas?",
            "Ano-ano ang pangunahing lenggwahe ng Pilipinas?",
            "Sino ang unang presidente ng Pilipinas?",
            "Sino ang unang presidente ng Commonwealth?",
            "Sa anong kontinente kabilang ang Pilipinas?", /*5*/
            "Ano ang kabisera ng Pilipinas?",
            "Pambansang awit ng Pilinas",
            "Pambansang hayop",
            "Pambansang isda",
            "Pambansang prutas", /*10*/
            "Pambansang bayani",
            "Pambansang bulaklak",
            "Pambansang puno",
            "Pambansang ibon",
            "Pambansang dahon", /*15*/
            "Pambansang bahay",
            "Pambansang kasuotan",
            "Unang nakadiskubre ng Pilipinas",
            "Ang Unang Digmaang Pandaigdig sa Pilipinas ay laban sa:",
            "Pangunahing relihiyon sa Pilipinas", /*20*/
            "Pambansang sayaw",
            "Ano sa pagpipilian ang hindi kabilang sa tatlong kumpol na pulo?",
            "Ilan ang sinag ng araw sa watawat ng Pilipinas?",
            "Ilan ang bituin sa watawat?",
            "Sino ang presidente ng Pilipinas sa kasalukuyan (2019)?", /*25*/
            "Ano ang pinakamalaking lungsod ng Pilipinas?",
            "Tawag sa mga naninirahan sa Pilipinas",
            "Ano ang uri ng pamahalaan mayroon ang Pilipinas?",
            "Pinakamataas na pinuno ng Pilipinas",
            "Pangunahing pagkain ng mga Pilipino", /*30*/
            /*Medium*/
            "Ano sa Filipino ang 'square'?",
            "Ano sa Filipino ang 'rectangle'?",
            "Ano sa Filipino ang 'red'?",
            "Kumpletuhin: Ang ganda ____ niya.",
            "Kumpletuhin: Kailangan nating magtulung-tulong ____ makamit natin ang La Nina.", /*5*/
            "Kumpletuhin: Tinuruan niya kami kung paano magbalat ____ patatas.",
            "Kumpletuhin: Ikot ____ ikot si Sarah G.",
            "Kumpletuhin: Hindi pa ____ daw makaalis ang sasakyan.",
            "Kumpletuhin: Ang hirap talaga kapag ____ ang alam natin sa kasaysayan",
            "Kumpletuhin: Nakalulungkot kong sabihin na wala ____ ang ating guro ngayong klase.", /*10*/
            "Kumpletuhin: Nasa EDSA ____ ako, bes.",
            "Kumpletuhin: Nakakainis ____ magbasa ng comment section.",
            "Ano sa Filipino ang 'grammar'?",
            "Awit na panghehele",
            "Korido na isinulat tungkol sa mahiwagang ibon.", /*15*/
            "Panitikang Pilipino na obra ng Balagtas",
            "Isalin sa Filipino ang katagang 'fall in line'.",
            "Suriin: Ang ama ay ang haligi ng tahanan.",
            "Ayaw lumabas ng bahay ni Fak, kapag naglalakad at nakarinig ng ihip ng hangin sa mga puno, lulundag ang puso at bibilis ang kaba. – Anong damdamin ang nangingibabaw sa pahayag?",
            "Suriin: Ang salitang yamang-dagat ay ____.",
            /*Hard*/
            "Ilang barko ang dala-dala ni Magellan noong nadiskubre niya ang Pilipinas?",
            "Saan itinayo ni Miguel Lopez de Legaspi ang San Miguel?",
            "Ilang taon nanatili ang mga Kastila sa Pilipinas?",
            "Taon nagsimula at nagtapos ang pananakop ng Kastila",
            "Kailan  ipinahayag ang Tagalog bilang pambansang wika ng Pilipinas?",
            "Ang kamatayan niya/nila ang naging mitsa ng kilusang propaganda.",
            "Unang editor ng La Solidaridad.",
            "Pinamulan niya ang 'Diariong Tagalog'.",
            "Sino ang presidente ng 'Associacion La Solidaridad'?",
            "Diyaryo ng mga Katipunero",
            "Pundamental na dokumentong turo ng Katipunan na isinulat ni Emilio Jacinto.",
            "'Taga-Ilog' ang sulat-pangalan ni",
            "Pinuno ng Magdiwang ng KKK sa Cavite",
            "May-akda ng 'Philippine Declaration of Independence'.",
            "Sino sa mga ito ang hindi kabilang na nagtahi ng watawat ng Pilipinas?"

    };

    public static final String[] answer = {
            "Piso",
            "Filipino at Ingles",
            "Emilio Aguinaldo",
            "Manuel Quezon",
            "Asya" /*5*/,
            "Manila",
            "Lupang Hinirang",
            "Kalabaw",
            "Bangus",
            "Mangga", /*10*/
            "Jose Rizal",
            "Sampaguita",
            "Narra",
            "Agila",
            "Anahaw", /*15*/
            "Bahay Kubo",
            "Barong Tagalog at Filipiniana",
            "Magellan",
            "Kastila",
            "Katoliko", /*20*/
            "Cariñosa",
            "Palawan",
            "Walo",
            "Tatlo",
            "Duterte", /*25*/
            "Lungsod ng Quezon",
            "Filipino/Filipina",
            "Republika",
            "Pangulo",
            "Kanin at Ulam",
            /*Medium*/
            "Parisukat",
            "Parihaba",
            "Pula",
            "ng",
            "nang", /*5*/
            "ng",
            "nang",
            "rin",
            "iba-iba",
            "na naman", /*10*/
            "pa lang",
            "palang",
            "Balarila",
            "Oyayi",
            "Ibong Adarna", /*15*/
            "Florante at Laura",
            "Pumila ng maayos",
            "Metapora",
            "Natatakot",
            "Tambalan", /*20*/
            /*Hard*/
            "5",
            "Cebu",
            "333 taon",
            "1565-1898",
            "Disyembre 30, 1937", /*5*/
            "GOMBURZA",
            "Graciano Lopez Jaena",
            "Marcelo Del Pilar",
            "Galiciano Apacible",
            "Kalayaan", /*10*/
            "Kartilya",
            "Antonio Luna",
            "Mariano Alvarez",
            "Ambrosio Rianzares Bautista",
            "Gregoria de Jesus"


    };

    public static final String[] incorrect1 = {
            "Dolyar",
            "Filipino at Espanyol",
            "Apolinario Mabini",
            "Manuel Roxas",
            "Australia", /*5*/
            "Cebu",
            "Bayang Magiliw",
            "Bangus",
            "Dilis",
            "Santol", /*10*/
            "Lapu-lapu",
            "Rosas",
            "Acacia",
            "Ibong Adarna",
            "Dahon ng Saging", /*15*/
            "Condo",
            "T-shirt at ripped jeans",
            "Lapu-lapu",
            "Hapon",
            "Iglesia ni Cristo", /*20*/
            "Budots",
            "Luzon",
            "Pito",
            "Apat",
            "Arroyo", /*25*/
            "Lungsod ng Maynila",
            "Kuya/Ate",
            "Diktaturya",
            "Ministro",
            "Mashed Potato", /*30*/
			/*Medium*/
            "Parihaba",
            "Bilog",
            "Asul",
            "nang",
            "ng", /*5*/
            "nang",
            "ng",
            "din",
            "iba iba",
            "nanaman", /*10*/
            "palang",
            "pa lang",
            "Baril-barilan",
            "Lullaby",
            "Mulawin", /*15*/
            "Ibong Adarna",
            "Hulog sa linya",
            "Personipikasyon",
            "Nakikiusap",
            "Payak", /*20*/
			/*Hard*/
            "10",
            "Bulacan",
            "123 taon",
            "1565 - 1764",
            "Nobyembre 12, 1937", /*5*/
            "Rizal",
            "Antonio Luna",
            "Juan Luna",
            "Jose Ma. Panganiban",
            "La Solidaridad", /*10*/
            "Diario de Filipinos",
            "Mariano Ponce",
            "Gregorio del Pilar",
            "Manuel Quezon",
            "Marcela Agoncillo"

    };

    public static final String[] incorrect2 = {
            "Euro",
            "Ingles at Bisaya",
            "Antonio Luna",
            "Sergio Osmena",
            "Europa", /*5*/
            "Davao",
            "Perlas ng Silanganan",
            "Tarsier",
            "Janitor Fish",
            "Niyog", /*10*/
            "Andres Bonifacio",
            "Sunflower",
            "Mahogany",
            "Maya",
            "Makahiya", /*15*/
            "Bungalow",
            "Cocktail dress at Tuxedo",
            "Abraham Lincoln",
            "Tsino",
            "Baptismo", /*20*/
            "Tinikling",
            "Mindanao",
            "Anim",
            "Isa",
            "Aquino", /*25*/
            "Lungsod ng Pasig",
            "Kyah/Teh",
            "Monarkiya",
            "Emperor",
            "Fried Chicken", /*30*/
            /*Medium*/
            "Oblong",
            "Tala",
            "Bughaw",
            "nangg",
            "nangg", /*5*/
            "nangg",
            "nangg",
            "drin",
            "ibaiba",
            "nananaman", /*10*/
            "pallang",
            "pallang",
            "Barirala",
            "Sesura",
            "Ravena", /*15*/
            "Noli Me Tangere",
            "Mahulog sa linya",
            "Hyperbole",
            "Naiinis",
            "Maylapi", /*20*/
            /*Hard*/
            "15",
            "Madrid",
            "352 taon",
            "1575 - 1909",
            "Hulyo 4, 1776",/*5*/
            "Bonifacio",
            "Jose Rizal",
            "Pedro Laktaw",
            "Mariano Ponce",
            "Diario de Manila", /*10*/
            "La Solidaridad",
            "Jose Ma. Panganiban",
            "Jose Ma. Panganiban",
            "Emilio Aguinaldo",
            "Lorenza Agoncillo" /*15*/

    };

    public static final String[] incorrect3 = {
            "Yen",
            "Ilokano at Waray",
            "Jose Rizal",
            "Carlos Garcia",
            "Amerika", /*5*/
            "Iloilo",
            "Pusong Bato",
            "Baka",
            "Maya-maya",
            "Buko", /*10*/
            "Cardo Dalisay",
            "Santan",
            "Balete",
            "Parrot",
            "Dahon ng Bayabas", /*15*/
            "Mansyon",
            "Wala",
            "Tsino",
            "Indones",
            "Saksi ni Jehovah", /*20*/
            "Cancan",
            "Visayas",
            "Sampu",
            "Wala",
            "Estrada", /*25*/
            "Lungsod ng Antipolo",
            "Papsh/Mamsh",
            "Oligarkiya",
            "Hari/Reyna",
            "Pasta", /*30*/
            /*Medium*/
            "Bilog",
            "Parisukat",
            "Dilaw",
            "Wala",
            "wala", /*5*/
            "wala",
            "wala",
            "wala",
            "wala",
            "wala", /*10*/
            "wala",
            "wala",
            "Balalala",
            "Opera",
            "Encantadong Ibon", /*15*/
            "El Filibusterismo",
            "Ihulog sa linya",
            "Simile",
            "Nangungulila",
            "Inuulit", /*20*/
            /*Hard*/
            "3",
            "Maynila",
            "Walang nakaaalam",
            "1762 - 1853",
            "Disyembre 13, 1989", /*5*/
            "Pilosopo Tasyo",
            "Apolinario Mabini",
            "Gregorio Sanciano",
            "Manuel Sta. Maria",
            "Diario ng Mamamayan", /*10*/
            "Wala",
            "Juan Luna",
            "Emilio Jacinto",
            "Antonio Luna",
            "Delfina Herbosa de Natividad" /*15*/

    };
}
