package com.caja.fritzgerald.elective4.Utils;

/**
 * Created by John Steven Frio on 12/15/2018.
 */

public class GameUtils {
    public static final String LOG_TAG = "ELEC4_TAG";

    //COUNT STARTING FROM 0
    public static final int NUM_OF_QUESTIONS = 64;

    public static final String SHAREDPREF_NAME = "TriviaGameSavedData";

    public static final String SHAREDPREFREVERSE_NAME = "ReverseSharedPref";

    public static final String SHAREDPREF_KEY_PROGRESS_ID = "progressId";

    public static final String SHAREDPREF_KEY_PROGRESS_DIFFICULTY = "progressDifficulty";



    public static final String SHAREDPREF_KEY_ITEMNO = "itemNo";



    public static final String BUNDLE_ISNEWGAME = "isNewGame";

    public static final String BUNDLE_DIFFICULTY = "difficulty";

}
