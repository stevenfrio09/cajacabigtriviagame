package com.caja.fritzgerald.elective4;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.card.MaterialCardView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.caja.fritzgerald.elective4.SQLiteAdapter.SQLiteAdapter;
import com.caja.fritzgerald.elective4.Utils.GameUtils;
import com.caja.fritzgerald.elective4.Utils.SharedPreferencesManager;
import com.caja.fritzgerald.elective4.Utils.ShuffleItems;

public class PlayActivity extends AppCompatActivity {

    TextView tv_questions, tv_difficulty, tv_option1, tv_option2, tv_option3, tv_option4, tv_itemNumber, tv_itemTotal;

    MaterialCardView mcv_overlay, mcv_option1, mcv_option2, mcv_option3, mcv_option4;

    SQLiteAdapter sqLiteAdapter;

    SharedPreferencesManager sharedPreferencesManager;

    ShuffleItems shuffleItems;

    int progressId;
    String progressDifficulty;

    int sharedPrefProgressId;
    String sharedPrefProgressDifficulty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        tv_questions = findViewById(R.id.tv_questions);
        tv_difficulty = findViewById(R.id.tv_difficulty);

        tv_option1 = findViewById(R.id.tv_option1);
        tv_option2 = findViewById(R.id.tv_option2);
        tv_option3 = findViewById(R.id.tv_option3);
        tv_option4 = findViewById(R.id.tv_option4);
        tv_itemNumber = findViewById(R.id.tv_itemNumber);
        tv_itemTotal = findViewById(R.id.tv_itemTotal);

        mcv_overlay = findViewById(R.id.mcv_overlay);
        mcv_option1 = findViewById(R.id.mcv_option1);
        mcv_option2 = findViewById(R.id.mcv_option2);
        mcv_option3 = findViewById(R.id.mcv_option3);
        mcv_option4 = findViewById(R.id.mcv_option4);

        sqLiteAdapter = new SQLiteAdapter(this);

        sharedPreferencesManager = new SharedPreferencesManager(this);

        shuffleItems = new ShuffleItems(PlayActivity.this);

        Bundle bundle = getIntent().getExtras();

        if (bundle.getBoolean(GameUtils.BUNDLE_ISNEWGAME)) {

            sharedPreferencesManager.clearSharedPreferences();

            progressId = 1;
            progressDifficulty = "easy";
            sharedPrefProgressId = 1;
            sharedPrefProgressDifficulty = "easy";

            sharedPreferencesManager.setProgressDifficulty(progressDifficulty);
            sharedPreferencesManager.setProgressId(progressId);

            shuffleQuestions();

        }else{
            shuffleQuestions();
        }


        getQAFromDB(sharedPreferencesManager.getProgressId(), sharedPreferencesManager.getProgressDifficulty());


        onClickListeners(tv_option1);
        onClickListeners(tv_option2);
        onClickListeners(tv_option3);
        onClickListeners(tv_option4);

    }

    private void getQAFromDB(int id, String progressDifficulty) {

        String question = sqLiteAdapter.selectQuestion(sharedPreferencesManager.getShuffledNumbers(id), progressDifficulty);
        String correct = sqLiteAdapter.selectCorrect(sharedPreferencesManager.getShuffledNumbers(id), progressDifficulty);
        String incorrect1 = sqLiteAdapter.selectIncorrect1(sharedPreferencesManager.getShuffledNumbers(id), progressDifficulty);
        String incorrect2 = sqLiteAdapter.selectIncorrect2(sharedPreferencesManager.getShuffledNumbers(id), progressDifficulty);
        String incorrect3 = sqLiteAdapter.selectIncorrect3(sharedPreferencesManager.getShuffledNumbers(id), progressDifficulty);

        Log.d(GameUtils.LOG_TAG, "question3 : " + question);
        Log.d(GameUtils.LOG_TAG, "correct3 : " + correct);

        Log.d(GameUtils.LOG_TAG, "progressId : " + sharedPreferencesManager.getProgressId());

        /*
        Log.d(GameUtils.LOG_TAG, "incorrect1 : " + incorrect1);
        Log.d(GameUtils.LOG_TAG, "incorrect2 : " + incorrect2);
        Log.d(GameUtils.LOG_TAG, "incorrect3 : " + incorrect3);
        */

        shuffleItems.shuffleChoices(correct, incorrect1, incorrect2, incorrect3, tv_option1, tv_option2, tv_option3, tv_option4);


        mcv_overlay.setVisibility(View.GONE);
        displayQA(sharedPreferencesManager.getProgressDifficulty(), question);
        displayItemCount();

    }

    private void displayItemCount() {

        tv_itemNumber.setText(String.valueOf(sharedPreferencesManager.getProgressId()));

       switch (sharedPreferencesManager.getProgressDifficulty()) {
            case "easy":
                tv_itemTotal.setText("10");
                break;
            case "medium":
                tv_itemTotal.setText("10");
                break;
            case "hard":
                tv_itemTotal.setText("5");
                break;
        }
    }

    private void shuffleQuestions() {
        int start = 0;
        int end = 0;
        int items = 0;

        switch (sharedPreferencesManager.getProgressDifficulty()) {
            case "easy":
                start = 0;
                end = 30;
                items = 30;
                break;
            case "medium":
                start = 0;
                end = 20;
                items = 20;
                break;
            case "hard":
                start = 0;
                end = 15;
                items = 15;
        }

        Log.d(GameUtils.LOG_TAG, "start : " + start);
        Log.d(GameUtils.LOG_TAG, "end : " + end);
        Log.d(GameUtils.LOG_TAG, "items : " + items);

        shuffleItems.shuffleQuestions(start, end, items, sharedPreferencesManager.getProgressDifficulty());


    }

    private void displayQA(String progressDifficulty, String question) {

        tv_difficulty.setText(progressDifficulty);
        tv_questions.setText(question);

    }

    private void onClickListeners(final TextView textView) {

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String playerAnswer = textView.getText().toString();

                String correctAnswer = sqLiteAdapter.selectCorrect(sharedPreferencesManager.getShuffledNumbers(sharedPreferencesManager.getProgressId()), sharedPreferencesManager.getProgressDifficulty());

                Log.d(GameUtils.LOG_TAG, "correctAnswerrrr : " + correctAnswer);

                if (!playerAnswer.equals(correctAnswer)) {

                    incorrectAnswer(correctAnswer);

                } else {

                    correctAnswer();
                }
            }
        });
    }

    private void correctAnswer() {
        if (sharedPreferencesManager.getProgressId() >= 10 && sharedPreferencesManager.getProgressDifficulty().equals("easy")) {
            sharedPreferencesManager.setProgressId(1);
            sharedPreferencesManager.setProgressDifficulty("medium");

            navigateToRoundCleared("easy");

            //shuffleQuestions();
            //getQAFromDB(sharedPreferencesManager.getProgressId(), sharedPreferencesManager.getProgressDifficulty());

        } else if (sharedPreferencesManager.getProgressId() >= 10 & sharedPreferencesManager.getProgressDifficulty().equals("medium")) {

            sharedPreferencesManager.setProgressId(1);
            sharedPreferencesManager.setProgressDifficulty("hard");

            navigateToRoundCleared("medium");

            //shuffleQuestions();
            //getQAFromDB(sharedPreferencesManager.getProgressId(), sharedPreferencesManager.getProgressDifficulty());

        } else if (sharedPreferencesManager.getProgressId() >= 5 & sharedPreferencesManager.getProgressDifficulty().equals("hard")) {

            navigateToRoundCleared("hard");

        } else {
            int incrementProgressId = sharedPreferencesManager.getProgressId() + 1;
            sharedPreferencesManager.setProgressId(incrementProgressId);
            getQAFromDB(sharedPreferencesManager.getProgressId(), sharedPreferencesManager.getProgressDifficulty());
        }
    }

    private void navigateToRoundCleared(String difficulty) {

        Intent intent = new Intent(PlayActivity.this, RoundCleared.class);
        Bundle bundle = new Bundle();
        bundle.putString(GameUtils.BUNDLE_DIFFICULTY, difficulty);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Quit and Save ?")
                .setMessage("Are you sure you want to quit ? Checkpoint will be saved..")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        saveProgress();

                        startActivity(new Intent(PlayActivity.this, MainActivity.class));
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    private void saveProgress() {
        //added '+1 to remove quit and resume glitch'
        sharedPreferencesManager.setProgressId(1 + 1);
        sharedPreferencesManager.setProgressDifficulty(sharedPreferencesManager.getProgressDifficulty());
    }

    private void incorrectAnswer(String correctAnswer) {

        if (tv_option1.getText().toString().equals(correctAnswer)) {

            animateView(mcv_option2, mcv_option3, mcv_option4, tv_option1);

        } else if (tv_option2.getText().toString().equals(correctAnswer)) {

            animateView(mcv_option1, mcv_option3, mcv_option4, tv_option2);

        } else if (tv_option3.getText().toString().equals(correctAnswer)) {

            animateView(mcv_option1, mcv_option2, mcv_option4, tv_option3);

        } else if (tv_option4.getText().toString().equals(correctAnswer)) {

            animateView(mcv_option1, mcv_option2, mcv_option3, tv_option4);
        }


/*
     */
    }

    private void animateView(final MaterialCardView mcv_optionX, final MaterialCardView mcv_optionY, final MaterialCardView mcv_optionZ, final TextView textView) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                mcv_optionX.setVisibility(View.GONE);
                mcv_optionY.setVisibility(View.GONE);
                mcv_optionZ.setVisibility(View.GONE);

                mcv_overlay.setVisibility(View.VISIBLE);

                textView.setClickable(false);

            }
        }, 500);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(PlayActivity.this);
                builder.setTitle("Game Over")
                        .setMessage("Aww. You are incorrect. ")
                        .setNegativeButton("Return to home", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                //sharedPreferencesManager.clearSharedPreferences();

                                startActivity(new Intent(PlayActivity.this, MainActivity.class));
                                finish();

                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        },500);

    }

    private void navigateToMainActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 3000);
    }
}
