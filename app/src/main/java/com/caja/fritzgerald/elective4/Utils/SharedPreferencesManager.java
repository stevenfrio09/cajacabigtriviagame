package com.caja.fritzgerald.elective4.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by John Steven Frio on 12/16/2018.
 */

public class SharedPreferencesManager {

    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public SharedPreferencesManager(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(GameUtils.SHAREDPREF_NAME, 0);
        editor = sharedPreferences.edit();
    }

    public void setProgressId(int progressId){
        editor.putInt(GameUtils.SHAREDPREF_KEY_PROGRESS_ID, progressId);
        editor.apply();
    }

    public Integer getProgressId(){
        return sharedPreferences.getInt(GameUtils.SHAREDPREF_KEY_PROGRESS_ID, 0);

    }

    public void setProgressDifficulty(String progressDifficulty){
        editor.putString(GameUtils.SHAREDPREF_KEY_PROGRESS_DIFFICULTY, progressDifficulty);
        editor.apply();
    }


    public String getProgressDifficulty(){
        return sharedPreferences.getString(GameUtils.SHAREDPREF_KEY_PROGRESS_DIFFICULTY, null);
    }

    public void setShuffledNumbers(int[] array){
        Log.d(GameUtils.LOG_TAG, "arrayLength : " + array.length);
        for(int i = 0; i< array.length;i++){
            int value = array[i];
            //Log.d(GameUtils.LOG_TAG, "generated numbers : " + s );
            editor.putInt(String.valueOf(i + 1), value);
            editor.apply();
        }

    }

    public Integer getShuffledNumbers(int key){
        return sharedPreferences.getInt(String.valueOf(key), 0);
    }


    public void clearSharedPreferences(){
        editor.clear();
        editor.apply();
    }

    public boolean isSharedPrefExisting(){
        boolean isExisting = false;

        if(sharedPreferences.contains(GameUtils.SHAREDPREF_KEY_PROGRESS_ID) || sharedPreferences.contains(GameUtils.SHAREDPREF_KEY_PROGRESS_DIFFICULTY)){
            isExisting = true;
        }

        return isExisting;

    }

}
