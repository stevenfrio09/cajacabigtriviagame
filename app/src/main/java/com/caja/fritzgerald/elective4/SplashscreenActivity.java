package com.caja.fritzgerald.elective4;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.caja.fritzgerald.elective4.SQLiteAdapter.SQLiteAdapter;
import com.caja.fritzgerald.elective4.TriviaQA.TriviaQA;
import com.caja.fritzgerald.elective4.Utils.GameUtils;

public class SplashscreenActivity extends AppCompatActivity {

    SQLiteAdapter sqLiteAdapter;
    TextView tv_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        deleteDatabase(SQLiteAdapter.SQLiteHelper.DATABASE_NAME);

        sqLiteAdapter = new SQLiteAdapter(this);

        tv_start = findViewById(R.id.tv_start);


        tv_start.setVisibility(View.INVISIBLE);
        tv_start.setEnabled(false);
        tv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashscreenActivity.this, MainActivity.class));
                finish();
            }
        });


        for(int i = 0; i<= GameUtils.NUM_OF_QUESTIONS; i++) {

            sqLiteAdapter.insertData(TriviaQA.difficulty[i], TriviaQA.questions[i], TriviaQA.answer[i], TriviaQA.incorrect1[i], TriviaQA.incorrect2[i], TriviaQA.incorrect3[i]);
            /*Log.d("ELEC4_TAG", "difficulty : " + TriviaQA.difficulty[i]);
            Log.d("ELEC4_TAG", "question : " + TriviaQA.questions[i]);
            Log.d("ELEC4_TAG", "answer : " + TriviaQA.answer[i]);
            Log.d("ELEC4_TAG", "incorrect1: " + TriviaQA.incorrect1[i]);
            Log.d("ELEC4_TAG", "incorrect2 : " + TriviaQA.incorrect2[i]);
            Log.d("ELEC4_TAG", "incorrect3 : " + TriviaQA.incorrect3[i]);*/

            //Log.d("ELEC4_TAG_SQL", String.valueOf(sqLiteAdapter.countRecords()));
            //Log.d("ELEC4_TAG_I", String.valueOf(i));

            if(i==GameUtils.NUM_OF_QUESTIONS){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        tv_start.setVisibility(View.VISIBLE);
                        tv_start.setEnabled(true);

                        Animation animation= new AlphaAnimation(0.0f, 1.0f);
                        animation.setDuration(400);
                        animation.setStartOffset(20);
                        animation.setRepeatMode(Animation.REVERSE);
                        animation.setRepeatCount(Animation.INFINITE);
                        tv_start.startAnimation(animation);

                    }
                }, 1500);
            }
        }


    }
}
